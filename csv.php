<?php
require 'server.php';

?>


<!DOCTYPE html>
<html>
<?php include "head.php" ?>
      <body>
      <?php include "header.php" ?>
      <form class="form-horizontal" action="" method="post" name="csv" style="margin-left: 25px;" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-md-4 control-label" style="font-size: 20px; margin-top: 15px;">Choose a CSV File</label><br>
            <input type="file" name="file" class="col-md-4 control-label" id="file" accept=".csv"><br>
        </div>
        <button type="submit" id="submit" name="import" style="margin-left: 15px;" class="btn btn-primary" class="btn-submit">Import</button>
        </form>
      </body>
</html>

<?php

// Importing csv file into the database

if (isset($_POST["import"])) {
    
    // First line = true(yes), comment it if no first line

    $flag = true;

    $fileName = $_FILES["file"]["tmp_name"];
    
    if ($_FILES["file"]["size"] > 0) {
        
        $file = fopen($fileName, "r");
        
        while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {

            // Ignores first line

            if($flag) { $flag = false; continue; }

            // Taking columns from inside the csv file - email, name, ip

            $stmt = $connection->prepare("INSERT INTO records (email, name, ip) VALUES ('" . $column[0] . "','" . $column[1] . "','" . $column[2] . "')");
            $stmt->execute();
            
            // Errors if anything fails

            if (!empty($result)) {
                $type = "success";
                $message = "CSV Data Imported into the Database";
            } else {
                $type = "error";
                $message = "Problem in Importing CSV Data";
            }
        }
    }

    // If successful show this message

    echo "<p style='margin-left: 40px; margin-top: 15px'>Imported!</p>";

}
?>